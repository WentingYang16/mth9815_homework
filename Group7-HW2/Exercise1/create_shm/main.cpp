// Create by Group 7

// Using boost::interprocess::shared_memory_object to create a shared_memory_object named "MySharedMemory"
// and create an integer named "MyInt" in it.



#include <iostream>
#include <boost/interprocess/managed_shared_memory.hpp>


int main()
{

    boost::interprocess::shared_memory_object::remove("MySharedMemory");
    boost::interprocess::managed_shared_memory managed_shm(boost::interprocess::open_or_create, "MySharedMemory", 100*sizeof(std::size_t));
    int *i = managed_shm.construct<int>("MyInt")(200);
    std::cout << "Int we create: "<<*i << "\n";
    return 0;
}

