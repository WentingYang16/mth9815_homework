// Create by Group 7

// Using boost::interprocess::shared_memory_object to create a shared_memory_object has the same name as "MySharedMemory"
// （we create it in the program "create_shm"）
// and find the integer named "MyInt" and print it on the screen.


#include <iostream>
#include <boost/interprocess/managed_shared_memory.hpp>
#include <utility>

int main()
{

    boost::interprocess::managed_shared_memory managed_shm(boost::interprocess::open_read_only, "MySharedMemory");
    std::pair<int*,std::size_t> ret = managed_shm.find<int>("MyInt");
    std::cout << "Int we get: "<< *ret.first << "; Size: " << ret.second << "\n";

    boost::interprocess::shared_memory_object::remove("MySharedMemory");
    return 0;
}
