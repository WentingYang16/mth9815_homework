// create by Group 7
// test the Future, EuroDollarFuture, BondFuture class
// test the productService classes

#include<boost/interprocess/shared_memory_object.hpp>
#include<boost/interprocess/mapped_region.hpp>
#include<cstring>
#include<cstdlib>
#include<string>
#include<iostream>
#include "product.hpp"
#include "productservice.hpp"
#include "soa.hpp"


int main()
{


	//question 2
	//future product 1
	double strike = 100;
	std::string ticker = "JBZ7";
	date maturityDate(2017, Dec, 12);
	int notional= 10000;
	std::string productID = "7690132";
    std::string underlying = "Equity";
	Future future1(strike, productID, ticker, maturityDate, notional,underlying);



	//future product 2
	double strike2 = 112;
	std::string ticker2 = "FVZ7";
	date maturityDate2(2017, Dec, 26);
	int notional2 = 10000;
	std::string productID2 = "7690123";
    std::string underlying2 = "Equity";
	Future future2(strike2, productID2, ticker2, maturityDate2, notional2,underlying2);

	//future product 3
	double strike3 = 123;
	std::string ticker3 = "GZ7";
	date maturityDate3(2017, Dec, 23);
	int notional3 = 10000;
	std::string productID3 = "7690312";
    std::string underlying3 = "Swap";
	Future future3(strike3, productID3, ticker3, maturityDate3, notional3,underlying3);

    std::cout<< future1 << "\n" ;
    std::cout << future2 << "\n";
    std::cout << future3 << "\n";


    // EuroDollarFuture product
    double strike4 = 100;
    std::string ticker4 = "EuroDollarFuture";
    date maturityDate4(2017, Dec, 24);
    int notional4 = 10000;
    std::string productID4 = "7700002";

    EuroDollarFuture EDFuture(strike4,productID4,ticker4,maturityDate4,notional4);

    std::cout << EDFuture <<"\n";

    // BondFuture product
    double strike5 = 100;
    std::string ticker5 = "BondFuture";
    date maturityDate5(2017, Dec, 22);
    int notional5 = 10000;
    std::string productID5 = "7700010";
    std::string underlying5 = "10yrTreasury";
    std::string currency = "USD";
    double fxrate = 0.02;
    BondFuture BFuture(strike5,productID5,ticker5,maturityDate5,notional5,underlying5,currency);

    std::cout << BFuture << " currency: "<< BFuture.getCurrency()<< "\n";




    //create the future product service
	FutureProductService *futureProductService = new FutureProductService();
	//add future1
	futureProductService->Add(future1);
	//get future1 by ID
	Future future = futureProductService->GetData(productID);
	std::cout << "Refcon: " << future.GetProductId() << std::endl;

	//add future2
	futureProductService->Add(future2);
	//get future2 by ID
	future = futureProductService->GetData(productID2);
	std::cout << "Ticker: " << future.GetTicker() << std::endl;

	//add future3
	futureProductService->Add(future3);
	//get future3 by ID
	future = futureProductService->GetData(productID3);
	std::cout << "Maturity Date: " << future.GetMaturity() << std::endl;

    std::vector<Future> same_notional = futureProductService->GetFutures(10000);
    for(auto i: same_notional)
        std::cout << i << "\n";

    return 0;
}
