//
// Created by Group 7 on 11/20/17.
//

#include "productservice.hpp"
#include <iostream>
#include <map>
#include <string>

FutureProductService::FutureProductService()
{
    futureMap = map<string, Future>();
}

Future& FutureProductService::GetData(string productId)
{
    return futureMap[productId];
}

void FutureProductService::Add(Future &future)
{
    futureMap.insert(pair<string, Future>(future.GetProductId(), future));
}


std::vector<Future> FutureProductService::GetFutures(std::string _ticker)
{
    std::vector<Future> res;
    for (auto itr =  futureMap.begin(); itr != futureMap.end(); itr++)
    {
        std::string tempTicker = itr->second.GetTicker();
        if (tempTicker == _ticker)
        {
            res.push_back(itr->second);
        }
    }
    return res;
}

std::vector<Future> FutureProductService::GetFutures(date _maturity)
{
    std::vector<Future> res;
    for (auto itr =  futureMap.begin(); itr != futureMap.end(); itr++)
    {
        date tempMaturity = itr->second.GetMaturity();
        if (tempMaturity == _maturity)
        {
            res.push_back(itr->second);
        }
    }
    return res;
}

std::vector<Future> FutureProductService::GetFutures(int _notional)
{
    std::vector<Future> res;
    for (auto itr =  futureMap.begin(); itr != futureMap.end(); itr++)
    {
        int tempNotional = itr->second.GetNotional();
        if (tempNotional == _notional)
        {
            res.push_back(itr->second);
        }
    }
    return res;
}


std::vector<Future> FutureProductService::GetFutures(double _strike)
{
    std::vector<Future> res;
    for (auto itr =  futureMap.begin(); itr != futureMap.end(); itr++)
    {
        double tempStrike = itr->second.GetStrike();
        if (tempStrike == _strike)
        {
            res.push_back(itr->second);
        }
    }
    return res;
}





BondProductService::BondProductService()
{
    bondMap = map<string, Bond>();
}

Bond& BondProductService::GetData(string productId)
{
    return bondMap[productId];
}

void BondProductService::Add(Bond &bond)
{
    bondMap.insert(pair<string, Bond>(bond.GetProductId(), bond));
}


//get all bonds with same bond tickers
std::vector<Bond> BondProductService::GetBonds(std::string _ticker)
{
    std::vector<Bond> res;
    for (auto itr = bondMap.begin(); itr != bondMap.end(); itr++)
    {
        std::string tempTicker = itr->second.GetTicker();
        if (tempTicker == _ticker)
        {
            res.push_back(itr->second);
        }
    }
    return res;
}













IRSwapProductService::IRSwapProductService()
{
    swapMap = map<string, IRSwap>();
}

IRSwap& IRSwapProductService::GetData(string productId)
{
    return swapMap[productId];
}

void IRSwapProductService::Add(IRSwap &swap)
{
    swapMap.insert(pair<string, IRSwap>(swap.GetProductId(), swap));
}


// Get all Swaps with the specified fixed leg day count convention
vector<IRSwap> IRSwapProductService::GetSwaps(DayCountConvention _fixedLegDayCountConvention)
{
    std::vector<IRSwap> res;
    for (auto itr = swapMap.begin(); itr != swapMap.end(); itr++)
    {
        auto temp = itr->second.GetFixedLegDayCountConvention();
        if (temp == _fixedLegDayCountConvention)
        {
            res.push_back(itr->second);
        }
    }
    return res;
}

// Get all Swaps with the specified fixed leg payment frequency
vector<IRSwap> IRSwapProductService::GetSwaps(PaymentFrequency _fixedLegPaymentFrequency)
{
    std::vector<IRSwap> res;
    for (auto itr = swapMap.begin(); itr != swapMap.end(); itr++)
    {
        auto temp = itr->second.GetFixedLegPaymentFrequency();
        if (temp == _fixedLegPaymentFrequency)
        {
            res.push_back(itr->second);
        }
    }
    return res;

}

// Get all Swaps with the specified floating index
vector<IRSwap> IRSwapProductService::GetSwaps(FloatingIndex _floatingIndex)
{
    std::vector<IRSwap> res;
    for (auto itr = swapMap.begin(); itr != swapMap.end(); itr++)
    {
        auto temp = itr->second.GetFloatingIndex();
        if (temp == _floatingIndex)
        {
            res.push_back(itr->second);
        }
    }
    return res;
}

// Get all Swaps with a term in years greater than the specified value
vector<IRSwap> IRSwapProductService::GetSwapsGreaterThan(int _termYears)
{
    std::vector<IRSwap> res;
    for (auto itr = swapMap.begin(); itr != swapMap.end(); itr++)
    {
        auto temp = itr->second.GetTermYears();
        if (temp > _termYears)
        {
            res.push_back(itr->second);
        }
    }
    return res;
}

// Get all Swaps with a term in years less than the specified value
vector<IRSwap> IRSwapProductService::GetSwapsLessThan(int _termYears)
{
    std::vector<IRSwap> res;
    for (auto itr = swapMap.begin(); itr != swapMap.end(); itr++)
    {
        auto temp = itr->second.GetTermYears();
        if (temp <_termYears)
        {
            res.push_back(itr->second);
        }
    }
    return res;

}

// Get all Swaps with the specified swap type
vector<IRSwap> IRSwapProductService::GetSwaps(SwapType _swapType)
{
    std::vector<IRSwap> res;
    for (auto itr = swapMap.begin(); itr != swapMap.end(); itr++)
    {
        auto temp = itr->second.GetSwapType();
        if (temp ==_swapType)
        {
            res.push_back(itr->second);
        }
    }
    return res;

}

// Get all Swaps with the specified swap leg type
vector<IRSwap> IRSwapProductService::GetSwaps(SwapLegType _swapLegType)
{
    std::vector<IRSwap> res;
    for (auto itr = swapMap.begin(); itr != swapMap.end(); itr++)
    {
        auto temp = itr->second.GetSwapLegType();
        if (temp ==_swapLegType)
        {
            res.push_back(itr->second);
        }
    }
    return res;

}
