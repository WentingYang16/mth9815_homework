#ifndef PRODUCTSERVICE_HPP
#define PRODUCTSERVICE_HPP


#include <iostream>
#include <map>
#include <string>
#include "product.hpp"
#include "soa.hpp"



/*
 * Modeling the FutureProductService
 */
class FutureProductService : public Service<string, Future>
{

public:
	// FutureProductService ctor
	FutureProductService();

	// Return the future data for a particular future product identifier
	Future& GetData(string productId) override;

	// Add a future to the service (convenience method)
	void Add(Future &future);

    // get all the futures of the same ticker
    std::vector<Future> GetFutures(std::string _ticker);

    // get all the futures of the same maturity
    std::vector<Future> GetFutures(date _maturity);


    // get all the futures of the same notional
    std::vector<Future> GetFutures(int _notional);

    // get all the futures of the same strike
    std::vector<Future> GetFutures(double _strike);




private:
	map<string, Future> futureMap; // cache of bond products

};


/**
* Bond Product Service to own reference data over a set of bond securities.
* Key is the productId string, value is a Bond.
*/
class BondProductService : public Service<string, Bond>
{

public:
	// BondProductService ctor
	BondProductService();

	// Return the bond data for a particular bond product identifier
	Bond& GetData(string productId) override;

	// Add a bond to the service (convenience method)
	void Add(Bond &bond);

	//Get all Bonds with the specified ticker
	std::vector<Bond> GetBonds(std::string _ticker);

private:
	map<string, Bond> bondMap; // cache of bond products

};

/**
* Interest Rate Swap Product Service to own reference data over a set of IR Swap products
* Key is the productId string, value is a IRSwap.
*/
class IRSwapProductService : public Service<string, IRSwap>
{
public:
	// IRSwapProductService ctor
	IRSwapProductService();

	// Return the IR Swap data for a particular bond product identifier
	IRSwap& GetData(string productId) override;

	// Add a bond to the service (convenience method)
	void Add(IRSwap &swap);

	// Get all Swaps with the specified fixed leg day count convention
	vector<IRSwap> GetSwaps(DayCountConvention _fixedLegDayCountConvention);

	// Get all Swaps with the specified fixed leg payment frequency
	vector<IRSwap> GetSwaps(PaymentFrequency _fixedLegPaymentFrequency);

	// Get all Swaps with the specified floating index
	vector<IRSwap> GetSwaps(FloatingIndex _floatingIndex);

	// Get all Swaps with a term in years greater than the specified value
	vector<IRSwap> GetSwapsGreaterThan(int _termYears);

	// Get all Swaps with a term in years less than the specified value
	vector<IRSwap> GetSwapsLessThan(int _termYears);

	// Get all Swaps with the specified swap type
	vector<IRSwap> GetSwaps(SwapType _swapType);

	// Get all Swaps with the specified swap leg type
	vector<IRSwap> GetSwaps(SwapLegType _swapLegType);




private:
	map<string, IRSwap> swapMap; // cache of IR Swap products

};







# endif

